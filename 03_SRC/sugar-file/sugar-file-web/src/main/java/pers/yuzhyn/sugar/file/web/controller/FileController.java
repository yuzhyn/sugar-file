package pers.yuzhyn.sugar.file.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/file")
public class FileController {

    @GetMapping("/getList")
    public Map<String,Object> getList(){
        Map<String,Object> result = new HashMap<>();
        result.put("name","tom and Jerry");
        result.put("age",20);
        result.put("address","山东省青岛市ABC");
        return result;
    }
}
