package pers.yuzhyn.sugar.file.lib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SugarFileLibApplication {

    public static void main(String[] args) {
        SpringApplication.run(SugarFileLibApplication.class, args);
    }

}
