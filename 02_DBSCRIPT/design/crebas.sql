/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2020/3/17 22:44:53                           */
/*==============================================================*/


drop table if exists download_log;

drop table if exists file_body;

drop table if exists file_head;

drop table if exists server_info;

drop table if exists user_info;

/*==============================================================*/
/* Table: download_log                                          */
/*==============================================================*/
create table download_log
(
   log_id               bigint not null,
   file_head_id         bigint,
   download_time        datetime,
   ip_address           varchar(15),
   primary key (log_id)
);

/*==============================================================*/
/* Table: file_body                                             */
/*==============================================================*/
create table file_body
(
   file_body_id         bigint not null,
   file_name            varchar(200),
   file_path            varchar(300),
   file_ext             varchar(20),
   create_time          datetime,
   file_size            bigint,
   file_md5             varchar(100),
   file_sha1            varchar(100),
   primary key (file_body_id)
);

/*==============================================================*/
/* Table: file_head                                             */
/*==============================================================*/
create table file_head
(
   file_head_id         bigint not null,
   file_body_id         bigint,
   user_id              bigint,
   server_id            bigint,
   upload_time          datetime,
   valid_term           datetime,
   valid_count          int,
   residue_count        int,
   need_download_key    boolean,
   primary key (file_head_id)
);

/*==============================================================*/
/* Table: server_info                                           */
/*==============================================================*/
create table server_info
(
   server_id            bigint not null,
   ip_address           varchar(15),
   mac_address          varchar(12),
   server_name          varchar(20),
   server_desc          varchar(200),
   create_time          datetime,
   update_time          datetime,
   primary key (server_id)
);

/*==============================================================*/
/* Table: user_info                                             */
/*==============================================================*/
create table user_info
(
   user_id              bigint not null,
   user_name            varchar(20),
   user_password        varchar(20),
   upload_key           varchar(100),
   download_key         varchar(100),
   file_count_limit     varchar(100),
   file_space_limit     varchar(100),
   file_extsize_limit   varchar(100),
   create_time          datetime,
   update_time          datetime,
   primary key (user_id)
);

